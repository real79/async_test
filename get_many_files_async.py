import asyncio
from aiohttp import ClientSession


async def task_get_file(client,url):
    async with client.get(url) as resp:
        assert resp.status == 200
        text=await resp.text()
        await client.close()
        return text

async def asynchronous(urls, max_download_files):
    tasks = [asyncio.ensure_future(task_get_file(ClientSession(), url)) for url in urls[:max_download_files]]
    del urls[:max_download_files]
    done, pending = await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)
    while urls:
        free_places_in_queue=min(max_download_files-len(pending),len(urls))
        tasks+=[asyncio.ensure_future(task_get_file(ClientSession(), url)) for url in urls[:free_places_in_queue]]
        del urls[:free_places_in_queue]
        done, pending = await asyncio.wait(tasks, return_when=asyncio.FIRST_COMPLETED)

    if pending:
        await asyncio.wait(tasks)


urls=['some_url_list']
ioloop = asyncio.get_event_loop()
ioloop.run_until_complete(asynchronous(urls,10))
ioloop.close()